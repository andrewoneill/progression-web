import {LogManager, computedFrom, autoinject} from 'aurelia-framework';
import {ProgressionData} from './services/progression-data';

@autoinject
export class Dashboard {
  private JSON = JSON;
  private progressionDbFile: FileList;
  public progressionDataModel: ProgressionData;
  public progressionData;
  public logger = LogManager.getLogger('welcome');
  public heading: string = 'Welcome to the Aurelia Navigation App!';

  public constructor(progressionDataModel: ProgressionData) {
    this.progressionDataModel = progressionDataModel;
    this.progressionData = this.progressionDataModel.getData();
  }

  /**
   * Returns a stringified version of the parsed progressionData
   * @return {string}
   */
  @computedFrom('progressionData')
  get workoutJson(): string {
    return this.JSON.stringify(this.progressionData);
  }

  /**
   * Reads in progressionDbFile once selected
   * @return {void}
   */
  public submit(event): void {
    let reader = new FileReader();
    reader.onload = ev => {
      let data = this.progressionDataModel.parseDataFromProgressionBackup(ev.target.result);
      this.progressionDataModel.getData(data);
    }
    reader.readAsText(this.progressionDbFile[0]);
  }
}
