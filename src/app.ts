import {autoinject} from 'aurelia-framework';
import {Router, RouterConfiguration} from 'aurelia-router';
import {ProgressionData} from './services/progression-data';
import Decimal from 'decimal.js';

@autoinject
export class App {
  public routerConfig: RouterConfiguration;
  public router: Router;
  public progressionDataModel: ProgressionData;

  constructor(progressionDataModel: ProgressionData) {
    this.progressionDataModel = progressionDataModel;
    this.progressionDataModel.getData();
    Decimal.config({ rounding: 2 });
  }

  public configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Aurelia';
    config.map([
      { route: ['', 'dashboard'], name: 'dashboard', moduleId: './dashboard',             nav: true, title: 'Dashboard' },
      { route: 'workouts',        name: 'workouts',  moduleId: './workouts/workout-list', nav: true, title: 'Workouts' }
      // { route: 'users',         name: 'users',        moduleId: 'users',        nav: true, title: 'Github Users' },
      // { route: 'child-router',  name: 'child-router', moduleId: 'child-router', nav: true, title: 'Child Router' }
    ]);

    this.router = router;
    this.routerConfig = config;
  }
}
