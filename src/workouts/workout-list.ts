import {LogManager, computedFrom, autoinject} from 'aurelia-framework';
import {ProgressionData} from '../services/progression-data';

@autoinject
export class WorkoutList {
  public progressionDataModel: ProgressionData;
  public progressionData;
  public currentWorkoutIndex = null;

  constructor(progressionDataModel: ProgressionData) {
    this.progressionDataModel = progressionDataModel;
    this.progressionData = this.progressionDataModel.data;
    console.log(this.progressionData);
  }

  public selectWorkout(index: number): void {
    this.currentWorkoutIndex = index;
  }
}
