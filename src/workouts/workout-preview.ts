import {customElement, computedFrom, bindable} from 'aurelia-framework';
import {ProgressionData} from '../services/progression-data';

@customElement('workout-preview')
export class WorkoutPreview {
  @bindable public workoutIndex: number;
  public progressionDataService: ProgressionData;
  public progressionData;

  constructor(progressionDataModel: ProgressionData) {
    this.progressionDataService = progressionDataModel;
    this.progressionData = this.progressionDataService.getData();
  }

  @computedFrom('workoutIndex')
  get currentWorkout(): Object {
    let workout = this.progressionDataService.getWorkoutByIndex(this.workoutIndex);
    console.log(workout);
    return workout;
  }
}
