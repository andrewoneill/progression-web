import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import Decimal from 'decimal.js';

@autoinject
export class ProgressionData {
  private JSON = JSON;
  public http: HttpClient;
  public data: Object = {};

  constructor(http: HttpClient) {
      this.http = http;
  }

  public getData(data?: Object) {
    if (data && Object.keys(data).length > 0) {
      Object.assign(this.data, data);
    }
    else if (Object.keys(this.data).length > 0) {
      return this.data;
    }
    else {
      this.http.fetch('2016-01-05 235442.progressionbackup')
      .then(response => {
        return response.text()
      }).then(text => {
        this.data = this.parseDataFromProgressionBackup(text);
      });
    }

  }

  // TODO
  public prevBestEst1RM(exercise): any {}

  /* Getter & Calculation Functions */

  get isMetric(): boolean {
    return this.data.preferences.unit;
  }

  public getWorkoutByIndex(index): Object {
    return this.data.workouts[index];
  }

  // Brzycki formula
  public est1RMforSet(set) {
    let onerm: number = set.weight / (1.0278 - (0.0278 * set.reps));
    return new Decimal(onerm.toFixed(10)).round();
  }

  public totalExerciseVolume(exercise): number {
    let volume: number = 0;
    for (let set of exercise.completedSets) {
      volume += (set.reps * set.weight);
    }
    return volume;
  }

  public totalExerciseReps(exercise): number {
    let reps: number = 0;
    for (let set of exercise.completedSets) {
      reps += set.reps;
    }
    return reps;
  }

  public totalExerciseSets(exercise): number {
    return exercise.completedSetCount;
  }

  public groupIdenticalSets(exercise): any {
    let combinedSets = [],
        currentCombination = null;
    for (let set of exercise.completedSets) {
      if (currentCombination === null) {
        currentCombination = set;
        currentCombination['count'] = 1;
      }
      else if (currentCombination.reps === set.reps
               && currentCombination.weight === set.weight) {
        currentCombination.count++;
      }
      else {
        combinedSets.push(currentCombination);
        currentCombination = set;
        currentCombination['count'] = 1;
      }
    }
    combinedSets.push(currentCombination);
    return combinedSets;
  }

  /* Functions to move into upload module */

  /**
   * Takes a ProgressionBackup (string representation of a JSON object) and parses it to create a ProgressionData object
   * @param  {string}           contents The string to be parsed
   * @param  {string}           encoding The encoding format of the string
   * @return {ProgressionData}  A ProgressionData object
   */
  public parseDataFromProgressionBackup(contents: string, encoding: string = 'Base64') {
    let data: Object = this.parsestring(contents, encoding);
    for (let key in data) {
      switch (key) {
        case 'ua.json':
          data['userExercises'] = data[key];
          break;
        case 'fws.json':
          data['workouts'] = data[key];
          break;
        case 'preferences_':
          data['preferences'] = data[key];
          break;
      }
      delete data[key];
    }
    return data;
    // this.progressionDataModel.populate(data);
    // return this.progressionDataModel;
  }

  /**
   * Parses base64 encoded JSON progressionDbFile output
   * @param  {string} file Base64 encoded string
   * @return {Object}      JSON Object
   */
  public parsestring(fileContents, encoding: string = 'Base64') {
    switch (encoding) {
      default:
        return this.parseJSON(atob(fileContents));
    }
  }

  /**
   * Recursively parses JSON stored as a string in an existing JSON object
   * @param  {string} value string of JSON object to be parsed
   * @return {Object}       Parsed JSON object
   */
  public parseJSON(value) {
    try {
      let obj = this.JSON.parse(value, (k, v) => {
        return typeof v === 'string' ? this.parseJSON(v) : v;
      });
      return obj;
    }
    catch (e) {
      return value;
    }
  }
}
