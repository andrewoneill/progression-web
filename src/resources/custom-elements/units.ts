import {bindable, customElement} from 'aurelia-framework';

@customElement('units')
export class Units {
  @bindable public measure: string = 'weight';
  @bindable public isMetric: boolean = true;

  get units(): string {
    if (this.isMetric) {
      switch (this.measure) {
        case 'length':
          return 'cm';
        default:
          return 'kg'
      }
    }
    else {
      switch (this.measure) {
        case 'length':
          return 'in';
        default:
          return 'lbs'
      }
    }
  }
}
