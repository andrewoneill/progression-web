import {autoinject, bindable, customElement} from 'aurelia-framework';

@customElement('barbell')
export class Barbell {
  @bindable public weight: number;
  @bindable public isMetric: boolean = true;
}
