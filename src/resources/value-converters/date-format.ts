import moment from 'moment';

export class DateFormatValueConverter {
  public toView(value, format = 'YYYY-MM-DD') {
    return moment(value).format(format);
  }
}
