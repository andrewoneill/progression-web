import moment from 'moment';
import Decimal from 'decimal.js';

export class TimeDifferenceValueConverter {
  public toView(startTime, endTime) {
    let a = moment(startTime);
    let b = moment(endTime);
    let duration = moment.duration(b.diff(a));
    let durationHours = duration.asHours();
    let durationMins = 0;
    if ( durationHours >= 1 ) {
      durationMins = duration.asMinutes() - (durationHours * 60);
      return new Decimal(durationHours.toFixed(10)).round() + ' hrs ' + new Decimal(durationMins.toFixed(10)).round() + ' mins';
    }
    else {
      durationMins = duration.asMinutes();
      return new Decimal(durationMins.toFixed(10)).round() + ' mins';
    }
  }
}
